from configparser import ConfigParser
import os
from common.DataAccessLayer.DataAccessLayer import LearningAccessor
from sparkSimulationMessageSequence.LearningPropertiesKey import LearningPropertiesKey

CONFIG_DEFAULT_SECTION = "default_section"


class LearningPropertiesReader(ConfigParser):
    """
    This class is responsible for reading and accessing the learning.properties file.
    TO-DO: Move this to common
    """

    def read_learning_properties(self, learning_properties_file_path):
        """
        This funciton reads the `learning.properties` file from existing models with default section
        :param learning_properties_file_path:
        :return:
        """
        assert os.path.isfile(learning_properties_file_path), "Learning properties file does not exists"

        with open(learning_properties_file_path, 'r') as fp:
            config_string = '[' + CONFIG_DEFAULT_SECTION + ']\n' + fp.read()
            self.read_string(config_string)

        # Check if learning.properties file marked this model as optimized
        is_optimized_model = self.get_property(LearningPropertiesKey.IS_OPTIMIZED_VERSION)
        if is_optimized_model == "TRUE":

            # Get the product, channel and goal
            product_uid = self.get_property(LearningPropertiesKey.PRODUCT_UID)
            channel_uid = self.get_property(LearningPropertiesKey.CHANNEL_UID)
            goal = self.get_property(LearningPropertiesKey.TARGET_TYPE)

            # Read the properties from database
            learning_accessor = LearningAccessor()
            optimal_parameters = learning_accessor.get_optimal_learning_parameters(product_uid, channel_uid, goal)

            # Iterate through the parameters and set the optimal configuration
            for optimal_parameter in optimal_parameters:
                param_name = optimal_parameter[0]
                param_value = optimal_parameter[1]
                self.set(CONFIG_DEFAULT_SECTION, param_name, param_value)

    def get_property(self, property_name):
        """
        This will return the value for the property name specified
        :param property_name:
        :return:
        """
        return self.get(CONFIG_DEFAULT_SECTION, property_name, fallback=None)
