##########################################################
##
##
## buildStaticFeatures
##
## description: build static design matrix for TTE
##
## created by : shirley.xu@aktana.com
## updated by : wendong.zhu@aktana.com
##              
## updated on : 2019-04-04
##
## Copyright AKTANA (c) 2019.
##
##
##########################################################
library(sparkLearning)
library(sparklyr)
library(dplyr)
library(openssl)
VAL_NO_DATA = 999.8
buildStaticFeatures <- function(prods, accountProduct) {
  
  if(length(prods)==0) { 
    prods <- "accountId"
  } else { 
    prods <- c("accountId",prods)
  }
  
  flog.info("Analyzing account product data")

  # prepare the accountProduct table and build design
  AP <- accountProduct
  AP <- AP %>% group_by(accountId) %>% mutate(temp=row_number(productName)) %>% ungroup() %>% filter(temp==1) %>% select(-temp)
  AP <- AP %>% select(one_of(c(prods)))
  
  if (sdf_ncol(AP) == 1) {
      # static matrix contains only accountId
      return (AP)
  }

  # change NA in string type column to "NA" so it will be counted as distinct count
  colTypes <- sdf_schema(AP)
  colTypes <- sapply(colTypes, function(x){x$type})
  chrV <- names(colTypes)[colTypes=="StringType"]
  
  AP <- AP %>% mutate_at(.vars=chrV, list(~ifelse(is.na(.),'NA',.)))
  AP <- AP %>% mutate_at(.vars=chrV, list(~ifelse(.=='','NA',.)))

  numV <- names(colTypes)[colTypes=="IntegerType"|colTypes=="DoubleType"] # pick out the non-character (numeric) variables
  AP <- AP %>% mutate_at(.vars=numV, list(~ifelse(is.na(.),as.double(VAL_NO_DATA),.)))
  
  AP <- sparkFilterOutColSameValue(AP)   # pick out variable withs more than 1 unique value

  originalColnames <- copy(tbl_vars(AP))
  originalColnames <- originalColnames[originalColnames!="accountId"]
  
  newColnames <- substr(md5(originalColnames), 1, 7) # hashinng col names to reduce long names
  
  predictorNamesAPColMap <- as.list(originalColnames)
  predictorNamesAPColMap <- setNames(predictorNamesAPColMap, as.vector(newColnames))

  # setnames(AP, old=as.character(predictorNamesAPColMap), new=names(predictorNamesAPColMap))
  new <- names(predictorNamesAPColMap)
  old <- as.character(predictorNamesAPColMap)
  AP <- dplyr::rename_at(AP, vars(old), ~ new) # change AP predictors to new names
  
  flog.info("return from buildStaticFeatures")
  
  return(list(AP=AP, predictorNamesAPColMap=predictorNamesAPColMap))
}
