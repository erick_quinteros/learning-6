##########################################################
##
##
## aktana-learning Install Aktana Learning Engines.
##
## description: Driver Code
## 1. 
##
## created by : marc.cohen@aktana.com
##
## created on : 2015-11-03
##
## Copyright AKTANA (c) 2015.
##
##
##########################################################

# TTE methods:
#   A - Covariates + Intervals + DoW (full model for DSE integration)
#   B - Covariates + Intervals (for reporting)
#   C - Covariates + DoW (for reporting)
METHODS <- c("A", "B", "C")  

ProductInteractionTypes <- c("SEND", "VISIT_DETAIL")
VAL_NO_DATA <- 999
NA_VALUE <- 999

messageTimingScoreModel <- function(con, con_l, runSettings, tteParams, whiteListDB)
{
    library(h2o)
    library(lattice)
    library(openxlsx)
    library(data.table)
    library(Hmisc)
    library(Learning)
  
    runStamp <- tteParams[["runStamp"]]
    runDir <- runSettings[["runDir"]]
    
    for (m in METHODS) {
        fle <- sprintf("%s/models_%s_%s",runDir, m, runStamp)                  # find the model directory
        if(!dir.exists(fle))
        {
            flog.warn("Model Directory not found: %s",fle);
            CATALOGENTY <<- sprintf("Model directory not found %s ---- %s",fle,CATALOGENTRY)
            return(list(1,NULL))
        }
    }

    fle <- sprintf("%s/messageTimingDriver.r_%s.xlsx",runDir,runStamp)
    if(!file.exists(fle))
    {
        flog.warn("XLSX file not found: %s",fle);
        CATALOGENTY <<- sprintf("XLSX file not found %s ---- %s",fle,CATALOGENTRY)
        return(list(1,NULL))
    }

    # retrieve tte parameters
    isReportOnly <- tteParams[["isReportOnly"]]
    LOOKBACK <- tteParams[["LOOKBACK"]]
    LOOKBACK_MAX <- tteParams[["LOOKBACK_MAX"]]
    predictAhead <- tteParams[["predictAhead"]]
    EventTypes <- tteParams[["EventTypes"]]
    predictRunDate <- tteParams[["predictRunDate"]]
    channels <- tteParams[["channels"]]

    prods <- tteParams[["prods"]]
    if(length(prods)==0)prods <- ""  # this is a kludge to fix defect in getConfigurationValue

    if (sum(prods %in% names(accountProduct)) < length(prods)) {
        print(prods)
        flog.warn("Predictors contain invalid ones, and cannot be used.")
        prods <- ""
    }

    if(prods=="") { prods <- "accountId" } else { prods <- c("accountId",prods) }
    
    pNameUID <- tteParams[["pNameUID"]]
    pName <- products[externalId==pNameUID]$productName

    sheetname.A <- sprintf("%s_A_reference", pName)
    models.A <- data.table(read.xlsx(fle,sheet=sheetname.A))  # for predictions

    sheetname.B <- sprintf("%s_B_reference", pName)
    models.B <- data.table(read.xlsx(fle,sheet=sheetname.B))  # for reporting

    sheetname.C <- sprintf("%s_C_reference", pName)
    models.C <- data.table(read.xlsx(fle,sheet=sheetname.C))  # for DoW output

    predictorNamesAPColMap <- list()
    if (length(prods)!=0) {
      # remove extra unnessary string <repAccountAttribute\:> added to learning.properties <LE_MS_addPredictorsFromAccountProduct> by DSE API & and save its original mapping for UI display
      predictorNamesAPColMap <- as.list(prods)
      prods <- gsub("repAccountAttribute\\:", "", prods, fixed=TRUE)
      predictorNamesAPColMap <- setNames(predictorNamesAPColMap, as.vector(prods))
      predictorNamesAPColMap <- list2env(predictorNamesAPColMap) # convert to hash for faster access
      
      # remove predictors not in AP
      prods <- prods[prods %in% colnames(accountProduct)]
    }
    
    flog.info("Build static design matrix ...")
##### first prepare the accountProduct table and build design
    AP <- accountProduct[, prods, with=F]
    print(describe(AP))                                           # summarize the accountproduct in the print file for debugging

    if (ncol(AP) >1) { # not just accountId column                                      
      AP <- AP[,sapply(AP,function(x)length(unique(x))>1),with=F]   # pick out variable withs more than 1 unique value
      tt<-data.table(sapply(AP,class))                              # find the classes of those variables
      tt$names<-names(AP)
      chrV <- tt[V1=="character"]                                   # pick out the character variables
      chrV <- chrV[names!="productName"]
      numV <- tt[V1!="character"]                                   # pick out the non-character (numeric) variables

      for(i in chrV$names)                                          # for the catgorical variables, assing missing values with 'NA'
      {                                                          
        eval(parse(text=sprintf("AP[is.na(%s),%s:='NA']",i,i)))
        eval(parse(text=sprintf("AP[(%s)=='', %s:='NA']",i,i)))
      }
      
      
      for (i in numV$names[-1]){
        AP[[i]][is.na(AP[[i]])] <- NA_VALUE
      }
      # only pick up first observation of same accountId
      AP <- subset(AP, !duplicated(accountId))
      
    }
##### done with static design
    
##### clean up memory
    rm(accountProduct,envir=parent.frame())                       # delete the accountProduct table since it's big to free up some memory
    rm(accountProduct,envir=globalenv())
    gc()

    flog.info("Build Design Matrix")

#### read dynamic model
    dynamic.model <- fread(sprintf("%s/dynamic.model.csv", runDir, BUILD_UID))
    dynamic.model[, date := as.Date(date)]

    # pick the last row in each accountId group
    dm <- dynamic.model[, .SD[.N], by=accountId][, .(accountId, numS, numV, pre2S, pre2V, preTARGET)]
    
#### next prepare to build design for dynamic predictors

    whiteList <- whiteListDB$accountId       # contains the accounts in the whitelist
    if(length(whiteList)==0){whiteList <- unique(interactions$accountId)} 
    
    ints <- interactions[accountId %in% whiteList]
    dm <- dm[accountId %in% whiteList]
    
    allTypes <- c(ProductInteractionTypes, EventTypes)
    ints <- ints[type %in% allTypes]          # filter interactions data using allTypes
    
    ints[type=="VISIT_DETAIL", type:="VISIT"]
    ints[type=="APPOINTMENT_DETAIL", type:="APPOINTMENT"]
    
    ints[,c("productName"):=NULL]
    
    flog.info("Preparing Design Matrix")
    flog.info("Size of ints %s %s",dim(ints)[1],dim(ints)[2])

    flog.info("Size of ints %s %s",dim(ints)[1],dim(ints)[2])
    if(dim(ints)[1]==0)
    {
        flog.error("No events in past %s days from %s.", LOOKBACK, predictRunDate)
        return(list(1,NULL))
    }
    
    setorder(ints, accountId, date)    # sort interaction data by accountId and date
    
    ## Now prepare data for scoring
    
    ints[, maxDate := max(date), by=c("accountId", "type")]
    ints[, c("physicalMessageUID") := NULL]     # TTE is not message-specific
    
    # get latest SEND/VISIT date for each account
    ints <- ints[date == maxDate]               # get only rows with latest date in interactions
    ints <- unique(ints)                        # remove duplicated rows
    ints$maxDate <- NULL

    if (isReportOnly || (runmodel == "REPORT")) {  # runmodel is passed from Rundeck job param
        flog.info("Running reporting job...")
        
        ttePredictReport(con, con_l, dynamic.model, segs, models.B, models.C, AP, ints,
                         RUN_UID, whiteList, tteParams)

        return (list(1, NULL))
      
    } else {
        # regular daily predictions on next 30 days
        ints[, diff := as.integer(predictRunDate - date)]      # calculate diff between today and latest interaction date
        setorder(ints, accountId, type, date)

        ints[diff <=0, diff := -1]                  # in case there are future dates (and today), set flag to -1
        
        ## prepare scoring data
        ints[, c("date")] <- NULL
        # now the data looks like:
        # accountId   type   diff
        #-------------------------
        # 1003        SEND    37
        # 1003        VISIT   58

        ints <- as.data.table(dcast(ints, accountId~type, sum))
        # now the data looks like: (0 means no date info on the event; -1 means today)
        # accountId   SEND   VISIT
        #-------------------------
        # 1003        37      58
        # 1003        4       0
        
        
        for (t in names(ints)[-1]){
          setnames(ints, t, paste("pre", t, sep=""))
        }




        # replace 0 (meaning no date info) with LOOKBACK_MAX, which means the date is pretty old. 
        
        ints[ints==0] <- LOOKBACK_MAX

        # replace -1 with 0

        ints[ints==-1] <- 0

        # get the numS, numV from dynamic model
        ints <- merge(ints, dm, by=c("accountId"), all.x = T)
        
        # replace NA with default value for numS, numV
        ints[is.na(numS), numS := 1]
        ints[is.na(numV), numV := 1]
        
        ints[is.na(pre2S), pre2S := VAL_NO_DATA]
        ints[is.na(pre2V), pre2V := VAL_NO_DATA]
        #ints[is.na(avgST), avgST := 0]   # because numS=1
        #ints[is.na(avgVT), avgVT := 0]
        
        ints[is.na(preTARGET), preTARGET := VAL_NO_DATA]
        
        # copy the data for S and V
        ints$ctr <- 1
        dup <- copy(ints)
        dup <- rbind(dup, ints[, ctr := 2])

        ints <- dup
        
        ints[ctr == 1, event := "S"]
        ints[ctr == 2, event := "V"]

        ints$ctr <- NULL
        
        setorder(ints, accountId, event)
        
        # add a column of prediction date with today's date
        ints$date <- predictRunDate
        
        # repeat for next predictAhead days
        dup  <- copy(ints)
        
        for(i in 1:(predictAhead-1)) {
          # Duplicate ints in order to predict future days
          # in each loop (one append), increase preS, preV, preT, etc. and date by 1
          
            for (j in names(ints)[-1]){
              if (stringr::str_detect(j, "pre") | j == "date"){
                set(ints, j=j, value=ints[[j]]+1)
              }
            }
            dup <- rbind(dup, ints)
        }
        
        setorder(dup, accountId, event, date)
        ints <- dup
        
        ## Method A for nightly predictions: Covariates + time between send & vist + DoW
        method <- "A"
        scores <- tteMethod(models.A, AP, ints, RUN_UID, method)
        scores$method <- "Covariates + Intervals + DoW"

        # variable dup, ints, AP are no longer needed, so force to release memory
        rm(dup)
        rm(ints)
        rm(AP)
        gc(); gc(); gc()

        flog.info("Return from scoreModel")
        return(list(0, scores))
    }
}
