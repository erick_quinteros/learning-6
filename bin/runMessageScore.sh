#!/bin/bash
#
# aktana-learning Run script for running an R script in R learning module.
#
# description: Learning modules are written in R. The script is used
#              to run appropriate scripts in R packages
#
# created by : satya.dhanushkodi@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#

# Set the learning home location if not set already
if [ -z "$LEARNING_HOME" ] ; then
   PRG="$0"
   LEARNING_HOME=`dirname "$PRG"`/..
fi

LOG_DIR=$LEARNING_HOME/../logs
DATA_DIR=$LEARNING_HOME/../data
LIB_DIR=$LEARNING_HOME/../library

while getopts m:r:h:s:u:p:c:d:e:b:g:i name
do
        case $name in
          m)MODULE=$OPTARG;;
          r)RSCRIPT=$OPTARG;;
          h)DB_HOST=$OPTARG;;
          s)DB_NAME=$OPTARG;;
          u)DB_USER=$OPTARG;;
          p)DB_PASSWORD=$OPTARG;;
          d)RUN_MODEL=$OPTARG;;
          c)CUSTOMER=$OPTARG;;
          e)ENV_NAME=$OPTARG;;
          b)BUILD_UID=$OPTARG;;
          g)CONFIG_UID=$OPTARG;;
          i)RUN_UID=$OPTARG;;
          *)echo "Invalid arg";;
        esac
done

# If module is not defined.
if [ -z "$MODULE" ] ; then
   echo "Error: MODULE (-m) is not defined correctly."
   exit 1
fi

# If rscript is not defined.
if [ -z "$RSCRIPT" ] ; then
   echo "Error: RSCRIPT (-r) is not defined correctly."
   exit 1
fi


# If dbhost is not defined.
if [ -z "$DB_HOST" ] ; then
   echo "Error: DB_HOST (-h) is not defined correctly."
   exit 1
fi

# If dbname is not defined.
if [ -z "$DB_NAME" ] ; then
   echo "Error: SCHEMA (DB_NAME) (-s) is not defined correctly."
   exit 1
fi


# If dbuser is not defined.
if [ -z "$DB_USER" ] ; then
   echo "Error: DB_USER (-u) is not defined correctly."
   exit 1
fi

# If dbpassword is not defined.
if [ -z "$DB_PASSWORD" ] ; then
   echo "Error: DB_PASSWORD (-p) is not defined correctly."
   exit 1
fi

# If customer is not defined.
if [ -z "$CUSTOMER" ] ; then
   echo "Error: CUSTOMER (-c) is not defined correctly."
   exit 1
fi

# If runmodel is not defined.
if [ -z "$RUN_MODEL" ] ; then
   echo "Error: RUN_MODEL (-d) is not defined correctly."
   exit 1
fi

# If BUILD_UID is not defined.
if [ -z "$BUILD_UID" ] ; then
   echo "Error: BUILD_UID (-b) is not defined correctly."
   exit 1
fi

# If CONFIG_UID is not defined.
if [ -z "$CONFIG_UID" ] ; then
   echo "Error: CONFIG_UID (-g) is not defined correctly."
   exit 1
fi

# If RUN_UID is not defined.
if [ -z "$RUN_UID" ] ; then
   echo "Error: RUN_UID (-i) is not defined correctly."
   exit 1
fi

# If environment path is not defined.
if [ -z "$ENV_NAME" ] ; then
   ENV_NAME='default';
fi

DB_HOST_PARAM='"'"$DB_HOST"'"'
DB_NAME_PARAM='"'"$DB_NAME"'"'
DB_USER_PARAM='"'"$DB_USER"'"'
DB_PASSWORD_PARAM='"'"$DB_PASSWORD"'"'
RUN_MODEL_PARAM='"'"$RUN_MODEL"'"'
CUSTOMER_PARAM='"'"$CUSTOMER"'"'
BUILD_UID_PARAM='"'"$BUILD_UID"'"'
CONFIG_UID_PARAM='"'"$CONFIG_UID"'"'
RUN_UID_PARAM='"'"$RUN_UID"'"'
ENVNAME_PARAM='"'"$ENV_NAME"'"'
RFULLSCRIPT="$RSCRIPT.r"
LOGNAME=$MODULE.$RSCRIPT.$ENV_NAME.stdout
LOG_DIR_PARAM='"'"$LOG_DIR"'"'
DATA_DIR_PARAM='"'"$DATA_DIR"'"'
HOME_DIR_PARAM='"'"$LEARNING_HOME"'"'
LIB_DIR_PARAM='"'"$LIB_DIR"'"'

RCMD="R CMD BATCH --no-save --no-restore "
RFILES="$LEARNING_HOME/$MODULE/$RFULLSCRIPT $LOG_DIR/$LOGNAME"
RARGS="--args dbuser=$DB_USER_PARAM dbpassword=$DB_PASSWORD_PARAM dbhost=$DB_HOST_PARAM dbname=$DB_NAME_PARAM BUILD_UID=$BUILD_UID_PARAM CONFIG_UID=$CONFIG_UID_PARAM RUN_UID=$RUN_UID_PARAM runmodel=$RUN_MODEL_PARAM libdir=$LIB_DIR_PARAM logdir=$LOG_DIR_PARAM datadir=$DATA_DIR_PARAM homedir=$HOME_DIR_PARAM envname=$ENVNAME_PARAM customer=$CUSTOMER_PARAM"

RFULLCMD="$RCMD '$RARGS ' $RFILES"

# Make sure log directory exists
mkdir -p $LOG_DIR
mkdir -p $DATA_DIR

# Verify if the module is in place.
if [ ! -f "$LEARNING_HOME/$MODULE/$RFULLSCRIPT" ] ; then
        echo "Error: Could not find $LEARNING_HOME/$MODULE/$RFULLSCRIPT"
        exit 1
fi

echo "running $MODULE/$RFULLSCRIPT"

echo $RFULLCMD
eval $RFULLCMD

rc=$?
echo $rc

case $rc in
  0) echo "done running $MODULE/$RFULLSCRIPT";;
  *) echo "failed running $MODULE/$RFULLSCRIPT";;
esac

exit $rc
