DROP TRIGGER IF EXISTS PullAccountInteractions;

CREATE TRIGGER PullAccountInteractions AFTER INSERT ON LEARNINGSCHEMA.Sent_Email_vod__c_arc
FOR EACH ROW
BEGIN
	INSERT INTO LEARNINGSCHEMA.SimulationAccountSentEmail (InteractionId, accountUID, messageUID, isOpen, status, clickCount, emailSubject, emailBody, emailSentDate, senderEmailID, accountEmailID, emailLastOpenedDate, lastClickDate, productUID, messageName, createdAt, updatedAt)
	SELECT NEW.Id, NEW.Account_vod__c, NEW.Approved_Email_Template_vod__c, NEW.Opened_vod__c, NEW.Status_vod__c, NEW.Click_Count_vod__c, NEW.Email_Config_Values_vod__c, NEW.Email_Content_vod__c,
	NEW.Email_Sent_Date_vod__c, NEW.Sender_Email_vod__c, NEW.Account_Email_vod__c, NEW.Last_Open_Date_vod__c, NEW.Last_Click_Date_vod__c, NEW.Product_vod__c,
	(SELECT messageName FROM STAGESCHEMA.AKT_StableMessage_PhysicalMessage WHERE physicalMessageUID = NEW.Approved_Email_Template_vod__c),
	CURRENT_TIMESTAMP, CURRENT_TIMESTAMP;
END;
