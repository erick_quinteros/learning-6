\name{getConfigurationValue}
\alias{getConfigurationValue}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{getCOnfigurationValue}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
getConfigurationValue(pname)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{pname}{
%%     ~~Describe \code{pname} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (pname) 
{
    if (dim(CONFIGURATION[parameterName == pname])[1] == 0) {
        flog.error("CONFIGURATION error. No configurationName \%s", 
            pname)
        return(0)
    }
    type <- CONFIGURATION[parameterName == pname]$parameterType
    value <- CONFIGURATION[parameterName == pname]$parameterValue
    flog.info("Configuration \%s \%s \%s", pname, type, value)
    if (sys.parent() == 0) 
        CATALOGENTRY <<- paste(CATALOGENTRY, " --- ", pname, 
            type, value)
    if (type == "string") 
        return(as.character(value))
    if (type == "numeric") 
        return(as.numeric(value))
    if (type == "stringList") 
        return(eval(parse(text = as.character(value))))
    if (type == "variable") 
        return(value)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
