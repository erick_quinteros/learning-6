\name{messageDesign}
\alias{messageDesign}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{messageDesign}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
messageDesign(ints, sendName, targetName)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{ints}{
%%     ~~Describe \code{ints} here~~
}
  \item{sendName}{
%%     ~~Describe \code{sendName} here~~
}
  \item{targetName}{
%%     ~~Describe \code{targetName} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
%%  ~~who you are~~
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (ints, sendName, targetName) 
{
    setkey(ints, accountId, date, key)
    accts <- unique(ints[key \%in\% c(sendName, targetName)]$accountId)
    ints <- unique(ints[accountId \%in\% accts])
    t <- EncodeMessages(ints, targetName)
    t <- as.data.table(dcast(t, recordId + accountId ~ value))
    t$recordId <- NULL
    return(t)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }% use one of  RShowDoc("KEYWORDS")
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
