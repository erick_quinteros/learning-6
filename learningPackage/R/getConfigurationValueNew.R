##########################################################
#
#
# LearningPackage Func: getConfigurationValueNew()
#
# description: adopt from original getConfigurationValue()
#              get parameter value from learing.properties
#              convert value to proper types if needed
#
# created by : shirley.xu@aktana.com
#
# created on : 2018-09-25
#
# Copyright AKTANA (c) 2018.
#
#
##########################################################

getConfigurationValueNew <- function(config, pname, convertFunc=NULL, defaultValue=0)
{
  # check where has this parameter in the config
  if(!(pname %in% names(config))) {
    flog.info("CONFIGURATION: No configurationName %s",pname)
    return(defaultValue)}
  # read config parameter
  value <- config[[pname]]
  # processing the value
  defaultDataTypeConversion <- function(value) {
    if(!is.na(suppressWarnings(as.numeric(value)))) {
      return(as.numeric(value))
    } else if (!is.na(as.logical(value))) {
      return (as.logical(value))
    } else if (length(grep(";",value))>0) {
      return(strsplit(value,";")[[1]])
    } else {
      return(value)
    }
  }
  if (is.null(convertFunc)) {
    return(defaultDataTypeConversion(value))
  } else {
    return(convertFunc(value))
  }
}

