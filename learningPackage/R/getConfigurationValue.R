#
#
# aktana-learning model building code for Aktana Learning Engines.
#
# description: This for extracting configuration info
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2015-10-13
#
# Copyright AKTANA (c) 2015.
#
##########################################################

getConfigurationValue <- function(pname)
    {
        if(!(pname %in% names(CONFIGURATION))){flog.info("CONFIGURATION: No configurationName %s",pname); return(0)}
        value <- CONFIGURATION[[pname]]
        if(!is.na(as.numeric(value)))return(as.numeric(value))
        if(length(grep(";",value))>0)return(strsplit(value,";")[[1]])
        if(is.na(as.numeric(value)))return(value)
    }

