context('testing calculateAnchor() in ANCHOR module')
print(Sys.time())

# set debug logging mode
library(futile.logger)
flog.threshold(DEBUG)

# load library and source script
library(Learning)
library(properties)
# library(data.table)
# setDTthreads(1) # IMPORTANT: to avoid error "OMP: Error #13: Assertion failure at kmp_runtime.cpp(6480)" causing by incompatiblity with data table and forEach
source(sprintf('%s/common/unitTest/initializeUnitTest.r',homedir))
source(sprintf("%s/anchor/code/calculateAnchor.r",homedir))
source(sprintf("%s/anchor/code/utils.r",homedir))

# load required inputs to functions
load(sprintf('%s/anchor/tests/data/from_loadAnchorData_INTERACTIONS_REP.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_loadAnchorData_future.RData', homedir))
buildUID <- readModuleConfig(homedir, 'anchor','buildUID')
config <- read.properties(sprintf('%s/anchor/tests/data/%s/learning.properties',homedir,buildUID))
predictRundate <- getConfigurationValueNew(config,"LE_AN_nightlyPredictRundate", convertFunc=as.Date)
numberCores <- getConfigurationValueNew(config,"LE_AN_numberCores")
predictAhead <- getConfigurationValueNew(config,"LE_AN_predictAhead") # predict how many days in the future
withinDayClusterRatio <- getConfigurationValueNew(config,"LE_AN_withinDayClusterRatio") # in learning.properties now, but not exposed to user
predictAheadDayList <- utils.generatePredictAheadDayList(predictRundate, predictAhead)
maxFarPercentile <- getConfigurationValueNew(config,"LE_AN_maxFarPercentile")
minIntsRequire <- getConfigurationValueNew(config,"LE_AN_minIntsRequire")
calAdprobThreshold <- getConfigurationValueNew(config,"LE_AN_calAdprobThreshold", convertFunc=as.numeric, defaultValue=0)
calAdconfThreshold <- getConfigurationValueNew(config,"LE_AN_calAdconfThreshold", convertFunc=as.numeric, defaultValue=0)
newRepList <- numeric(0)

# run function for calendarAdherenceOn=FALSE
calendarAdherenceOn <- FALSE
repCalendarAdherence <- NULL
repAccountCalendarAdherence <- NULL
temp <- calculateAnchor(INTERACTIONS_REP, future, predictRundate, predictAheadDayList, numberCores, withinDayClusterRatio, maxFarPercentile, minIntsRequire, calendarAdherenceOn, repCalendarAdherence, repAccountCalendarAdherence, calAdprobThreshold, calAdconfThreshold)
result_new <- temp[["result"]]
newRepList_new <- temp[["newRepList"]]
rm(temp)

# after validating that new data is correct, uncomment to update unit test data
# result <- result_new
# save(result, file=sprintf('%s/anchor/tests/data/from_calculateAnchor.RData', homedir))

# test cases
test_that("result has the correct dimension and save as samed", {
  expect_equal(dim(result_new),c(570,12))
  load(sprintf('%s/anchor/tests/data/from_calculateAnchor_result_calAdOff.RData', homedir))
  expect_equal(result_new, result)
})

test_that("if have calendar, will not have the same facilityId in history", {
  result_new[,totalN:= .N, by=list(repId,date,facilityId)]
  result_new[,sourceN:= .N, by=list(repId,date,facilityId,source)]
  processed_result <- result_new[(source=="calendar") | (source=="history" & totalN==sourceN), colnames(result_new), with=F]
  expect_equal(processed_result, result_new)
})

test_that("newRepList has the correct dimension and save as samed", {
  expect_equal(dim(newRepList_new), c(1,3))
  load(sprintf('%s/anchor/tests/data/from_calculateAnchor_newRepList.RData', homedir))
  expect_equal(newRepList_new, newRepList)
})

# run calculate anchor with calendarAdherenceOn=TRUE
calendarAdherenceOn <- TRUE
load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repCalendarAdherence.RData', homedir))
load(sprintf('%s/anchor/tests/data/from_calculateCalendarAdherence_repAccountCalendarAdherence.RData', homedir))
temp <- calculateAnchor(INTERACTIONS_REP, future, predictRundate, predictAheadDayList, numberCores, withinDayClusterRatio, maxFarPercentile, minIntsRequire, calendarAdherenceOn, repCalendarAdherence, repAccountCalendarAdherence, calAdprobThreshold, calAdconfThreshold)
result_new <- temp[["result"]]
newRepList_new <- temp[["newRepList"]]
rm(temp)

# test cases
test_that("result has the correct dimension and save as samed with calendarAdherenceOn=TRUE", {
  expect_equal(dim(result_new),c(570,12))
  load(sprintf('%s/anchor/tests/data/from_calculateAnchor_result.RData', homedir))
  expect_equal(result_new, result)
})

test_that("if have calendar, will not have the same facilityId in history with calendarAdherenceOn=TRUE", {
  result_new[,totalN:= .N, by=list(repId,date,facilityId)]
  result_new[,sourceN:= .N, by=list(repId,date,facilityId,source)]
  processed_result <- result_new[(source=="calendar") | (source=="history" & totalN==sourceN), colnames(result_new), with=F]
  expect_equal(processed_result, result_new)
})

test_that("newRepList has the correct dimension and save as samed with calendarAdherenceOn=TRUE", {
  expect_equal(dim(newRepList_new), c(1,3))
  load(sprintf('%s/anchor/tests/data/from_calculateAnchor_newRepList.RData', homedir))
  expect_equal(newRepList_new, newRepList)
})

