import unittest
from unittest.mock import patch
import sys
import os
import logging
import pandas as pd
from pyspark import SparkContext
from pyspark.sql import SparkSession,SQLContext
from pyspark.sql.functions import udf
from pyspark.sql.types import *
from datetime import datetime #, timedelta
from dateutil.relativedelta import *
from freezegun import freeze_time

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
module_dir = os.path.dirname(script_dir)
learning_dir = os.path.dirname(module_dir)
sys.path.append(learning_dir)

from common.unitTest.pyunittestUtils import unitTestParent
from common.pyUtils.database_utils import DatabasePoolConnection, other_database_op
from common.DataAccessLayer.SparkDataAccessLayer import initialize, CommonDbAccessor, SparkDSEAccessor, SparkLearningAccessor, SparkCSAccessor, SparkStageAccessor
from common.DataAccessLayer.DataAccessLayer import connect_learning_database, connect_dse_database
from common.DataAccessLayer.DatabaseConfig import DatabaseConfig
from sparkRepEngagementCalculator.RepEngagementCalculator import RepEngagementCalculator
from sparkRepEngagementCalculator.RepEngagementCalculator import main
from common.pyUtils.logger import get_module_logger
from common.logger.Logger import create_logger
from references import schemas


import tracemalloc


logger = get_module_logger("TEST_REP_ENGAGEMENT_CALCULATION")

class test_RepEngagementCalculator(unitTestParent):

    def setUp(self):
        # logging.disable(logging.CRITICAL)
        self.orig_argv = sys.argv
        self.classPrefix = "sparkRepEngagementCalculator.RepEngagementCalculator.RepEngagementCalculator"
        super().setUp()
        self.dbconfig = DatabaseConfig.instance()
        self.dbconfig.set_config(self.db_host, self.db_user, self.db_password, self.db_port, self.dse_db_name, self.learning_db_name, self.cs_db_name, self.stage_db_name)

    def tearDown(self):
        sys.argv = self.orig_argv
        super().tearDown()
        logging.disable(logging.NOTSET)
        # v = input("")

class test_RepEngagementCalculatorClass(test_RepEngagementCalculator):

    def init_spark(self):
        calc = RepEngagementCalculator(sys.argv[1][9:-1], self.dbconfig)
        spark = SparkSession.builder.appName("TestRepEngagementCalculators").getOrCreate()
        sc = spark.sparkContext
        initialize("RepEngagementCalculatorLogger", spark)
        return spark, calc

    def compare_df(self, df1, df2):
        df1_list = df1.collect()
        df2_list = df2.collect()
        # print(df1_list)
        # print(df2_list)
        for row in df1_list:
            if row in df2_list:
                continue
            return False
        return True

    def spark_to_pd(self, spark_df):
        pd_df = spark_df.toPandas()
        return pd_df

    def pd_to_spark(self, spark, pd_df, schema = None):
        if schema == None:
            spark_df = spark.createDataFrame(pd_df)
        else:
            spark_df = spark.createDataFrame(pd_df, schema)
        return spark_df

    def test_init(self):
        #tests that the class is initialized with the expected values
        calc = RepEngagementCalculator(sys.argv[1][9:-1], self.dbconfig)
        self.assertEqual(calc.learning_home_dir, sys.argv[1][9:-1])
        self.assertEqual(calc.database_config, self.dbconfig)

        self.assertEqual(calc.dse_db_name, self.dbconfig.dse_db_name)
        self.assertEqual(calc.db_learning, self.dbconfig.learning_db_name)

        self.assertIsInstance(calc.dse_accessor, SparkDSEAccessor)
        self.assertIsInstance(calc.learning_accessor, SparkLearningAccessor)
        self.assertIsInstance(calc.cs_accessor, SparkCSAccessor)
        self.assertIsInstance(calc.stage_accessor, SparkStageAccessor)

    def test_format_dates(self):
        spark, calc = self.init_spark()

        #tests when date_past is 2 years in the past
        #function inputs
        today = datetime(2020, 1, 1, 12, 00, 1)
        date_past = datetime(2018, 1, 1, 12, 00, 1)

        #expected outputs
        start_date_stringReference = " where startDateLocal >= '2018-01-01'"
        end_date_stringReference = " and startDateLocal <= '2020-01-01'"
        published_suggestions_conditionReference = "WHERE DR.startDateLocal BETWEEN '2018-01-01 00:00:00' AND '2020-01-01 23:59:59' "
        synched_Suggestions_conditionReference = "AND A.Sync_Completed_Datetime_vod__c > '2018-01-01 00:00:00'"

        #testing outputs
        start_date_string, end_date_string, published_suggestions_condition, synched_Suggestions_condition = calc.format_dates(today, date_past)
        self.assertEqual(start_date_string, start_date_stringReference)
        self.assertEqual(end_date_string, end_date_stringReference)
        self.assertEqual(published_suggestions_condition, published_suggestions_conditionReference)
        self.assertEqual(synched_Suggestions_condition, synched_Suggestions_conditionReference)

        #tests when date_past is 2 months in the past
        #function inputs
        today = datetime(2020, 1, 1, 12, 00, 1)
        date_past = datetime(2019, 11, 1, 12, 00, 1)

        #expected outputs
        start_date_stringReference = " where startDateLocal >= '2019-11-01'"
        end_date_stringReference = " and startDateLocal <= '2020-01-01'"
        published_suggestions_conditionReference = "WHERE DR.startDateLocal BETWEEN '2019-11-01 00:00:00' AND '2020-01-01 23:59:59' "
        synched_Suggestions_conditionReference = "AND A.Sync_Completed_Datetime_vod__c > '2019-11-01 00:00:00'"

        #testing outputs
        start_date_string, end_date_string, published_suggestions_condition, synched_Suggestions_condition = calc.format_dates(today, date_past)
        self.assertEqual(start_date_string, start_date_stringReference)
        self.assertEqual(end_date_string, end_date_stringReference)
        self.assertEqual(published_suggestions_condition, published_suggestions_conditionReference)
        self.assertEqual(synched_Suggestions_condition, synched_Suggestions_conditionReference)

    def test_get_initial_tables(self):
        spark, calc = self.init_spark()
        with patch.object(calc.dse_accessor, "get_run_df"):
            with patch.object(calc.dse_accessor, "get_run_rep_date_df"):
                with patch.object(calc.dse_accessor, "get_spark_run_df"):
                    with patch.object(calc.dse_accessor, "get_spark_run_rep_date_df"):
                        calc.dse_accessor.get_run_df.return_value = [1]
                        calc.dse_accessor.get_run_rep_date_df.return_value = [1]
                        calc.dse_accessor.get_spark_run_df.return_value = [1]
                        calc.dse_accessor.get_spark_run_rep_date_df.return_value = [1]
                        calc.get_initial_tables(1)
                        calc.dse_accessor.get_run_df.assert_called()
                        calc.dse_accessor.get_run_rep_date_df.assert_called()
                        calc.dse_accessor.get_spark_run_df.assert_called()
                        calc.dse_accessor.get_spark_run_rep_date_df.assert_called()

    def test_join_DSErun_DSErunrepdate(self):
        spark, calc = self.init_spark()

        #function inputs
        # from reference import DSERunSchema
        DSERun = self.pd_to_spark(spark, super().get_table("DSERun", self.dse_db_name), schemas.dserun)
        # from reference import DSERunRepDateSchema
        DSERunRepDate = self.pd_to_spark(spark, super().get_table("DSERunRepDate", self.dse_db_name), schemas.dseRunRepDate)

        #expected outputs
        final_df_oneReference = spark.read.schema(schemas.final_df_one).json("sparkRepEngagementCalculator/tests/references/final_df_one.json")
        nextPublishedDateReference = udf(lambda startDateTime_old, startDateTime_new: startDateTime_new.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime_old < startDateTime_new) else None)

        #testing outputs
        final_df_one, nextPublishedDate = calc.join_DSErun_DSErunrepdate(DSERun, DSERunRepDate)
        final_df_one = final_df_one.select("runId", "startDateLocal", "repId", "startDateTime", "publishedDate")
        final_df_oneReference = final_df_oneReference.select("runId", "startDateLocal", "repId", "startDateTime", "publishedDate")
        self.assertEqual(True, self.compare_df(final_df_oneReference, final_df_one))

    def test_calculate_next_published_date(self):
        spark, calc = self.init_spark()

        #first testing when is_spark is false
        #function inputs
        final_df_oneReference = spark.read.schema(schemas.final_df_one).json("sparkRepEngagementCalculator/tests/references/final_df_one.json")
        is_spark = 0

        #expected outputs
        nextPublishedReference = spark.read.schema(schemas.nextPublished).json("sparkRepEngagementCalculator/tests/references/nextPublished.json")
        nextPublishedDate = udf(lambda startDateTime_old, startDateTime_new: startDateTime_new.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime_old < startDateTime_new) else None)

        #testing outputs
        nextPublished = calc.calculate_next_published_date(final_df_oneReference, is_spark, nextPublishedDate)
        nextPublishedReference = nextPublishedReference.select("repId", "runId", "publishedDate", "startDateLocal", "nextPublishedDate")
        nextPublished = nextPublished.select("repId", "runId", "publishedDate", "startDateLocal", "nextPublishedDate")
        self.assertEqual(True, self.compare_df(nextPublishedReference, nextPublished))

        # then testing when is_spark is true
        #function inputs
        # from references import final_df_onesparkSchema
        # final_df_one_sparkReferenceSchema = final_df_onesparkSchema.schema
        final_df_one_sparkReference = spark.read.schema(schemas.final_df_oneSPARK).json("sparkRepEngagementCalculator/tests/references/final_df_oneSPARK.json")
        is_spark = 1

        #expected outputs - the same outputs as expected when is_spark = 0

        #testing outputs
        nextPublished = calc.calculate_next_published_date(final_df_one_sparkReference, is_spark, nextPublishedDate)
        nextPublishedReference = nextPublishedReference.select("repId", "runId", "publishedDate", "startDateLocal", "nextPublishedDate")
        nextPublished = nextPublished.select("repId", "runId", "publishedDate", "startDateLocal", "nextPublishedDate")
        self.assertEqual(True, self.compare_df(nextPublishedReference, nextPublished))

    def test_join_spark_DSErun_DSErunrepdate(self):
        spark, calc = self.init_spark()

        #function inputs
        # from reference import DSERunSchema
        SparkDSERun = self.pd_to_spark(spark, super().get_table("DSERun", self.dse_db_name), schemas.dserun)
        # from reference import DSERunRepDateSchema
        SparkDSERunRepDate = self.pd_to_spark(spark, super().get_table("DSERunRepDate", self.dse_db_name), schemas.dseRunRepDateSPARK)

        #expected outputs
        final_df_one_sparkReference = spark.read.schema(schemas.final_df_oneSPARK).json("sparkRepEngagementCalculator/tests/references/final_df_oneSPARK.json")
        nextPublishedDateReference = udf(lambda startDateTime_old, startDateTime_new: startDateTime_new.strftime("%Y-%m-%d %H:%M:%S") if (startDateTime_old < startDateTime_new) else None)

        #testing outputs
        final_df_one_spark, nextPublished = calc.join_spark_DSErun_DSErunrepdate(SparkDSERun, SparkDSERunRepDate)
        final_df_one_spark = final_df_one_spark.select("runUID", "repId", "startDateLocal", "startDateTime", "publishedDate")
        final_df_one_sparkReference = final_df_one_sparkReference.select("runUID", "repId", "startDateLocal", "startDateTime", "publishedDate")
        self.assertEqual(True, self.compare_df(final_df_one_sparkReference, final_df_one_spark))

    def test_merge_published_dates(self):
        spark, calc = self.init_spark()

        #function inputs
        nextPublishedReference = spark.read.schema(schemas.nextPublished).json("sparkRepEngagementCalculator/tests/references/nextPublished.json")
        nextPublished_sparkReference = spark.read.schema(schemas.nextPublished).json("sparkRepEngagementCalculator/tests/references/nextPublished.json")

        #expected outputs
        nextPublished_totalReference = spark.read.schema(schemas.nextPublished).json("sparkRepEngagementCalculator/tests/references/nextPublished_total.json")

        #testing outputs
        nextPublished_total = calc.merge_published_dates(nextPublishedReference, nextPublished_sparkReference)
        nextPublished_total = nextPublished_total.select("repId", "runId", "publishedDate", "startDateLocal", "nextPublishedDate")
        nextPublished_totalReference = nextPublished_totalReference.select("repId", "runId", "publishedDate", "startDateLocal", "nextPublishedDate")
        self.assertEqual(True, self.compare_df(nextPublished_totalReference, nextPublished_total))

    def test_get_rep_table(self):
        spark, calc = self.init_spark()
        with patch.object(calc.dse_accessor, "get_rep_df"):
            calc.dse_accessor.get_rep_df.return_value = [1]
            calc.get_rep_table()
            calc.dse_accessor.get_rep_df.assert_called()

    def test_merge_next_published_with_rep(self):
        spark, calc = self.init_spark()

        #function inputs
        nextPublished_totalReference = spark.read.schema(schemas.nextPublished).json("sparkRepEngagementCalculator/tests/references/nextPublished_total.json")
        repReference = self.pd_to_spark(spark, super().get_table("Rep", self.dse_db_name), schemas.rep)

        #expected outputs
        next_published_totReference = spark.read.schema(schemas.next_published_tot).json("sparkRepEngagementCalculator/tests/references/next_published_tot.json")

        #testing outputs
        next_published_tot = calc.merge_next_published_with_rep(nextPublished_totalReference, repReference)
        next_published_tot = next_published_tot.select("repId", "runId", "publishedDate", "nextPublishedDate", "externalId", "startDateLocal")
        next_published_totReference = next_published_totReference.select("repId", "runId", "publishedDate", "nextPublishedDate", "externalId", "startDateLocal")
        self.assertEqual(True, self.compare_df(next_published_totReference, next_published_tot))

    def tests_get_sync_info(self):
        spark, calc = self.init_spark()
        with patch.object(calc.cs_accessor, "get_sync_info_table"):
            calc.cs_accessor.get_sync_info_table.return_value = [1]
            sync_info = calc.get_sync_info(1)
            calc.cs_accessor.get_sync_info_table.assert_called()

    def tests_merge_sync_info_with_externalID(self):
        spark, calc = self.init_spark()

        #function inputs
        next_published_totReference = spark.read.schema(schemas.next_published_tot).json("sparkRepEngagementCalculator/tests/references/next_published_tot.json")
        sync_trackingReference = self.pd_to_spark(spark, super().get_table("Sync_Tracking_vod__c", self.cs_db_name), schemas.sync_tracking)

        #expected outputs
        sync_infoReference = spark.read.schema(schemas.sync_info).json("sparkRepEngagementCalculator/tests/references/sync_info_df.json")

        #testing outputs
        sync_info = calc.merge_sync_info_with_externalID(next_published_totReference, sync_trackingReference)
        sync_infoReference = sync_infoReference.select("externalId", "syncStart")
        sync_info = sync_info.select("externalId", "syncStart")
        self.assertEqual(True, self.compare_df(sync_infoReference, sync_info))

    def test_merge_published_suggestions_with_syncs(self):
        spark, calc = self.init_spark()

        #function inputs
        next_published_totReference = spark.read.schema(schemas.next_published_tot).json("sparkRepEngagementCalculator/tests/references/next_published_tot.json")
        sync_infoReference = spark.read.schema(schemas.sync_info).json("sparkRepEngagementCalculator/tests/references/sync_info_df.json")

        #expected outputs
        synced_runsReference = spark.read.schema(schemas.synced_runs).json("sparkRepEngagementCalculator/tests/references/synced_runs_df.json")

        #testing outputs
        synced_runs = calc.merge_published_suggestions_with_syncs(next_published_totReference, sync_infoReference)
        synced_runsReference = synced_runsReference.select("repId", "StartDateLocalSynched", "runId", "Times_Synched")
        synced_runs = synced_runs.select("repId", "StartDateLocalSynched", "runId", "Times_Synched")
        self.assertEqual(True, self.compare_df(synced_runsReference, synced_runs))

    @patch.object(RepEngagementCalculator, "format_dates", return_value = [1,2,3,4])
    @patch.object(RepEngagementCalculator, "get_initial_tables", return_value = [1,2,3,4])
    @patch.object(RepEngagementCalculator, "join_DSErun_DSErunrepdate", return_value = [1,2])
    @patch.object(RepEngagementCalculator, "calculate_next_published_date", return_value = [1])
    @patch.object(RepEngagementCalculator, "join_spark_DSErun_DSErunrepdate", return_value = [1,2])
    @patch.object(RepEngagementCalculator, "merge_published_dates", return_value = [1])
    @patch.object(RepEngagementCalculator, "get_rep_table", return_value = [1])
    @patch.object(RepEngagementCalculator, "merge_next_published_with_rep", return_value = [1])
    @patch.object(RepEngagementCalculator, "get_sync_info", return_value = [1])
    @patch.object(RepEngagementCalculator, "merge_sync_info_with_externalID", return_value = [1])
    @patch.object(RepEngagementCalculator, "merge_published_suggestions_with_syncs", return_value = [1])
    def test_initialize_df_empty_table(self, mock_merge_published_suggestions_with_syncs,
        mock_merge_sync_info_with_externalID, mock_get_sync_info, \
        mock_merge_next_published_with_rep, mock_get_rep_table, \
        mock_merge_published_dates, mock_join_spark_DSErun_DSErunrepdate, \
        mock_calculate_next_published_date, mock_join_DSErun_DSErunrepdate, \
        mock_get_initial_tables, mock_format_dates):
        spark, calc = self.init_spark()
        #testing initialize_df when the provided RepEngagementCalculation table is empty
        field = [StructField("FIELDNAME_1",StringType(), True)]
        schema = StructType(field)
        mock_rep_engagement_table = spark.createDataFrame([], schema)
        with patch.object(calc.learning_accessor, "get_rep_engagement_table") as mock_get_rep_engagement_table:
            #function inputs
            mock_get_rep_engagement_table.return_value = mock_rep_engagement_table
            freezer = freeze_time("2020-01-01 12:00:01")
            freezer.start()
            calc.initialize_df()
            freezer.stop()

            #expected outputs
            freezer = freeze_time("2020-1-1 12:00:00")
            freezer.start()
            date_one = datetime.now()
            freezer.stop()
            freezer = freeze_time("2018-1-1 12:00:00")
            freezer.start()
            date_two = datetime.now()
            freezer.stop()

            #testing outputs
            mock_format_dates.assert_called_with(date_one, date_two)
            mock_get_initial_tables.assert_called()
            mock_merge_published_suggestions_with_syncs.assert_called()
            mock_merge_sync_info_with_externalID.assert_called()
            mock_get_sync_info.assert_called()
            mock_merge_next_published_with_rep.assert_called()
            mock_get_rep_table.assert_called()
            mock_merge_published_dates.assert_called()
            mock_join_spark_DSErun_DSErunrepdate.assert_called()
            mock_calculate_next_published_date.assert_called()
            mock_join_DSErun_DSErunrepdate.assert_called()

        #testing when the provided RepEngagement table is not empty
        field = [StructField("FIELDNAME_1",StringType(), True)]
        schema = StructType(field)
        mock_rep_engagement_table = spark.createDataFrame([("a",)], schema)
        with patch.object(calc.learning_accessor, "get_rep_engagement_table") as mock_get_rep_engagement_table:
            #function inputs
            mock_get_rep_engagement_table.return_value = mock_rep_engagement_table
            freezer = freeze_time("2020-01-01 12:00:01")
            freezer.start()
            calc.initialize_df()
            freezer.stop()

            #expected outputs
            freezer = freeze_time("2020-1-1 12:00:00")
            freezer.start()
            date_one = datetime.now()
            freezer.stop()
            freezer = freeze_time("2019-11-1 12:00:00")
            freezer.start()
            date_two = datetime.now()
            freezer.stop()

            #testing outputs
            mock_format_dates.assert_called_with(date_one, date_two)
            mock_get_initial_tables.assert_called()
            mock_merge_published_suggestions_with_syncs.assert_called()
            mock_merge_sync_info_with_externalID.assert_called()
            mock_get_sync_info.assert_called()
            mock_merge_next_published_with_rep.assert_called()
            mock_get_rep_table.assert_called()
            mock_merge_published_dates.assert_called()
            mock_join_spark_DSErun_DSErunrepdate.assert_called()
            mock_calculate_next_published_date.assert_called()
            mock_join_DSErun_DSErunrepdate.assert_called()

    def test_get_AKT_RepLicense_arc(self):
        spark, calc = self.init_spark()
        with patch.object(calc.stage_accessor, "get_AKT_rep_license_arc"):
            calc.stage_accessor.get_AKT_rep_license_arc.return_value = [1]
            calc.get_AKT_RepLicense_arc()
            calc.stage_accessor.get_AKT_rep_license_arc.assert_called()

    def test_get_rpt_dim_calendar(self):
        spark, calc = self.init_spark()
        with patch.object(calc.stage_accessor, "get_rpt_dim_calendar"):
            calc.stage_accessor.get_rpt_dim_calendar.return_value = [1]
            calc.get_rpt_dim_calendar()
            calc.stage_accessor.get_rpt_dim_calendar.assert_called()

    def test_get_rpt_Suggestion_Delivered_stg(self):
        spark, calc = self.init_spark()

        #function inputs
        synced_runsReference = spark.read.schema(schemas.synced_runs).json("sparkRepEngagementCalculator/tests/references/synced_runs_df.json")
        start_date_stringReference = " where startDateLocal >= '2018-01-01'"
        end_date_stringReference = " and startDateLocal <= '2020-01-01'"
        suggestion_Delivered_RawReference = self.pd_to_spark(spark, super().get_table("RPT_Suggestion_Delivered_stg", self.stage_db_name), schemas.suggestion_delivered_raw)

        #expected outputs
        suggestion_Delivered_syncedReference = spark.read.schema(schemas.suggestion_Delivered_synced).json("sparkRepEngagementCalculator/tests/references/suggestion_Delivered_synced.json")

        #testing outputs
        with patch.object(calc.stage_accessor, "get_rpt_Suggestion_Delivered_stg") as mock_get_rpt_Suggestion_Delivered_stg:
            mock_get_rpt_Suggestion_Delivered_stg.return_value = suggestion_Delivered_RawReference
            suggestion_Delivered_synced = calc.get_rpt_Suggestion_Delivered_stg(start_date_stringReference, end_date_stringReference, synced_runsReference)
            suggestion_Delivered_synced = suggestion_Delivered_synced.select("repId", "runId", "externalId", "suggestionLifeCycleId", "runUID", "repTeamId", "repTeamUID", "repTeamName", "seConfigId", "seConfigName", "runGroupId", "suggestedDate", "startDateLocal", "repUID", "repName", "repCreatedAt", "accountId", "accountUID", "runRepDateSuggestionId", "accountName", "detailRepActionTypeId", "detailRepActionTypeUID", "detailRepActionName", "runRepDateSuggestionDetailId", "productId", "productUID", "productName", "messageId", "messageUID", "messageName", "suggestionUID", "suggestionReferenceId", "lastViewedAt", "viewedAt", "viewedDuration", "actionTaken", "actionTaken_dt", "isSuggestionCompleted", "isSuggestionCompletedDirect", "isSuggestionCompletedInfer", "isSuggestionDismissed", "dismissReasonType", "dismissReason", "dismissReason_dt", "isSuggestionActive", "facilityLatitude", "facilityLongitude", "facilityGeoLocationString", "repLatitude", "repLongitude", "territoryCityName", "districtName", "territoryId", "territoryName", "regionName", "regionGroup", "dismissedAt", "dismissCount", "lastPublishedAt", "createdAt", "updatedAt", "reportedInteractionUID", "inferredInteractionUID", "interactionUID", "completedAt", "inferredAt", "isFirstSuggestedDate", "isLastSuggestedDate", "suggestionDriver", "timeoffday", "isHighestRunidForDay", "holiday_weekend_Flag", "crmFieldName", "reasonText", "reasonRank", "runRepDateSuggestionReasonId", "repRole", "repBag", "dmUID", "dmName", "interactionId", "isCompleted", "startDateTime", "isREMix", "isDSESpark", "arc_createdAt", "arc_updatedAt", "StartDateLocalSynched", "Times_Synched")
            suggestion_Delivered_syncedReference = suggestion_Delivered_syncedReference.select("repId", "runId", "externalId", "suggestionLifeCycleId", "runUID", "repTeamId", "repTeamUID", "repTeamName", "seConfigId", "seConfigName", "runGroupId", "suggestedDate", "startDateLocal", "repUID", "repName", "repCreatedAt", "accountId", "accountUID", "runRepDateSuggestionId", "accountName", "detailRepActionTypeId", "detailRepActionTypeUID", "detailRepActionName", "runRepDateSuggestionDetailId", "productId", "productUID", "productName", "messageId", "messageUID", "messageName", "suggestionUID", "suggestionReferenceId", "lastViewedAt", "viewedAt", "viewedDuration", "actionTaken", "actionTaken_dt", "isSuggestionCompleted", "isSuggestionCompletedDirect", "isSuggestionCompletedInfer", "isSuggestionDismissed", "dismissReasonType", "dismissReason", "dismissReason_dt", "isSuggestionActive", "facilityLatitude", "facilityLongitude", "facilityGeoLocationString", "repLatitude", "repLongitude", "territoryCityName", "districtName", "territoryId", "territoryName", "regionName", "regionGroup", "dismissedAt", "dismissCount", "lastPublishedAt", "createdAt", "updatedAt", "reportedInteractionUID", "inferredInteractionUID", "interactionUID", "completedAt", "inferredAt", "isFirstSuggestedDate", "isLastSuggestedDate", "suggestionDriver", "timeoffday", "isHighestRunidForDay", "holiday_weekend_Flag", "crmFieldName", "reasonText", "reasonRank", "runRepDateSuggestionReasonId", "repRole", "repBag", "dmUID", "dmName", "interactionId", "isCompleted", "startDateTime", "isREMix", "isDSESpark", "arc_createdAt", "arc_updatedAt", "StartDateLocalSynched", "Times_Synched")
            self.assertEqual(True, self.compare_df(suggestion_Delivered_synced, suggestion_Delivered_syncedReference))

    def test_merge_suggestions_delivered_with_AKT_RepLicense_arc(self):
        spark, calc = self.init_spark()

        #function inputs
        suggestion_Delivered_syncedReference = spark.read.schema(schemas.suggestion_Delivered_synced).json("sparkRepEngagementCalculator/tests/references/suggestion_Delivered_synced.json")
        # suggestions_df1Reference = spark.read.schema(schemas.suggestions_df1).json("sparkRepEngagementCalculator/tests/references/suggestions_df1.json")
        # suggestion_Delivered_dfReference.show()
        repLicenceTable_dfReference = spark.read.schema(schemas.repLicenceTable).json("sparkRepEngagementCalculator/tests/references/repLicenceTable_df.json")

        #expected outputs
        SuggDel_RepLicenceReference = spark.read.schema(schemas.SuggDel_RepLicence).json("sparkRepEngagementCalculator/tests/references/SuggDel_RepLicence_df.json")

        #testing outputs
        SuggDel_RepLicence = calc.merge_suggestions_delivered_with_AKT_RepLicense_arc(suggestion_Delivered_syncedReference, repLicenceTable_dfReference)
        self.assertEqual(True, self.compare_df(SuggDel_RepLicence, SuggDel_RepLicenceReference))

    def test_merge_suggestions_delivered_RepLicense_with_rpt_dim(self):
        spark, calc = self.init_spark()

        #function inputs
        SuggDel_RepLicence_dfReference = spark.read.schema(schemas.SuggDel_RepLicence).json("sparkRepEngagementCalculator/tests/references/SuggDel_RepLicence_df.json")
        dim_calendar_dfReference = self.pd_to_spark(spark, super().get_table("rpt_dim_calendar", self.stage_db_name), schemas.dim_calendar)
        # dim_calendar_dfReference = spark.read.schema(schemas.dim_calendar).json("sparkRepEngagementCalculator/tests/references/dim_calendar_df.json")

        #expected outputs
        suggestions_df1Reference = spark.read.schema(schemas.suggestions_df1).json("sparkRepEngagementCalculator/tests/references/suggestions_df1.json")

        #testing outputs
        suggestions_df1 = calc.merge_suggestions_delivered_RepLicense_with_rpt_dim(SuggDel_RepLicence_dfReference, dim_calendar_dfReference)
        suggestions_df1 = suggestions_df1.select("startDateLocal", "repUID", "repName", "cluster", "seConfigId", "seConfigName", "suggestionDriver", "suggestionReferenceId", "territoryId", "territoryName", "suggestedDate", "actionTaken", "datefield", "day_of_week", "calendar_year", "calendar_quarter", "quarter_value", "month_of_year", "day_of_month", "week_label", "week_value", "month_value", "month_value_full", "month_label", "is_holiday", "is_weekend", "is_last_day_of_month")
        suggestions_df1Reference = suggestions_df1Reference.select("startDateLocal", "repUID", "repName", "cluster", "seConfigId", "seConfigName", "suggestionDriver", "suggestionReferenceId", "territoryId", "territoryName", "suggestedDate", "actionTaken", "datefield", "day_of_week", "calendar_year", "calendar_quarter", "quarter_value", "month_of_year", "day_of_month", "week_label", "week_value", "month_value", "month_value_full", "month_label", "is_holiday", "is_weekend", "is_last_day_of_month")
        # suggestions_df1.show()
        # suggestions_df1Reference.show()
        self.assertEqual(True, self.compare_df(suggestions_df1, suggestions_df1Reference))
        return

    def test_filter_holidays(self):
        spark, calc = self.init_spark()

        #function inputs
        suggestions_df1Reference = spark.read.schema(schemas.suggestions_df1).json("sparkRepEngagementCalculator/tests/references/suggestions_df1.json")

        #expected outputs
        suggestions_df1Reference = spark.read.schema(schemas.suggestions_df1).json("sparkRepEngagementCalculator/tests/references/suggestions_df1.json")

        #testing outputs
        suggestions_df1 = calc.filter_holidays(suggestions_df1Reference)
        self.assertEqual(True, self.compare_df(suggestions_df1, suggestions_df1Reference))

    def test_add_colums_df(self):
        spark, calc = self.init_spark()

        #function inputs
        suggestions_df1Reference = spark.read.schema(schemas.suggestions_df1).json("sparkRepEngagementCalculator/tests/references/suggestions_df1.json")

        #expected outputs
        suggestions_df2Reference = spark.read.schema(schemas.suggestions_df2).json("sparkRepEngagementCalculator/tests/references/suggestions_df2.json")

        #testing outputs
        suggestions_df2, udf_cluster = calc.add_colums_df(suggestions_df1Reference)
        self.assertEqual(True, self.compare_df(suggestions_df2, suggestions_df2Reference))

    def test_suggestions_delivered_calculation(self):
        spark, calc = self.init_spark()

        #function inputs
        suggestions_df2Reference = spark.read.schema(schemas.suggestions_df2).json("sparkRepEngagementCalculator/tests/references/suggestions_df2.json")

        #expected outputs
        suggestion_deliveredReference = spark.read.schema(schemas.suggestion_delivered).json("sparkRepEngagementCalculator/tests/references/suggestion_Delivered_df.json")

        #testing outputs
        suggestion_delivered = calc.suggestions_delivered_calculation(suggestions_df2Reference)
        self.assertEqual(True, self.compare_df(suggestion_delivered, suggestion_deliveredReference))

    def test_engaged_suggestions_calculations(self):
        spark, calc = self.init_spark()

        #function inputs
        suggestions_df2Reference = spark.read.schema(schemas.suggestions_df2).json("sparkRepEngagementCalculator/tests/references/suggestions_df2.json")

        #expected outputs
        engaged_suggestionReference = spark.read.schema(schemas.engaged_suggestions).json("sparkRepEngagementCalculator/tests/references/engaged_suggestions_df.json")

        #testing outputs
        engaged_suggestion = calc.engaged_suggestions_calculations(suggestions_df2Reference)
        self.assertEqual(True, self.compare_df(engaged_suggestion, engaged_suggestionReference))

    def test_merge_suggestions_delivered_with_engaged(self):
        spark, calc = self.init_spark()

        #function inputs
        suggestion_deliveredReference = spark.read.schema(schemas.suggestion_delivered).json("sparkRepEngagementCalculator/tests/references/suggestion_Delivered_df.json")
        engaged_suggestionReference = spark.read.schema(schemas.engaged_suggestions).json("sparkRepEngagementCalculator/tests/references/engaged_suggestions_df.json")
        repLicenceTable_dfReference = spark.read.schema(schemas.repLicenceTable).json("sparkRepEngagementCalculator/tests/references/repLicenceTable_df.json")
        udf_cluster_upperReference = udf(lambda x: x.upper())

        #expected outputs
        final_dfReference = spark.read.schema(schemas.final_df).json("sparkRepEngagementCalculator/tests/references/final_df.json")


        #testing outputs
        final_df = calc.merge_suggestions_delivered_with_engaged(udf_cluster_upperReference, suggestion_deliveredReference, engaged_suggestionReference, repLicenceTable_dfReference)

    @patch.object(RepEngagementCalculator, "get_AKT_RepLicense_arc", return_value = [1])
    @patch.object(RepEngagementCalculator, "get_rpt_dim_calendar", return_value = [1])
    @patch.object(RepEngagementCalculator, "merge_suggestions_delivered_with_AKT_RepLicense_arc", return_value = [1])
    @patch.object(RepEngagementCalculator, "merge_suggestions_delivered_RepLicense_with_rpt_dim", return_value = [1])
    @patch.object(RepEngagementCalculator, "filter_holidays", return_value = [1])
    @patch.object(RepEngagementCalculator, "add_colums_df", return_value = [1, 2])
    @patch.object(RepEngagementCalculator, "engaged_suggestions_calculations", return_value = [1])
    @patch.object(RepEngagementCalculator, "merge_suggestions_delivered_with_engaged", return_value = [1])
    @patch.object(RepEngagementCalculator, "suggestions_delivered_calculation", return_value = [1])
    def test_finalize_df(self, mock_merge_suggestions_delivered_with_engaged, \
        mock_engaged_suggestions_calculations, mock_suggestions_delivered_calculation, \
        mock_add_colums_df, mock_filter_holidays, \
        mock_merge_suggestions_delivered_RepLicense_with_rpt_dim, \
        mock_merge_suggestions_delivered_with_AKT_RepLicense_arc, \
        mock_get_rpt_dim_calendar, mock_get_AKT_RepLicense_arc):
        spark, calc = self.init_spark()
        #function inputs
        field = [StructField("FIELDNAME_1",StringType(), True)]
        schema = StructType(field)
        mock_table = spark.createDataFrame([], schema)
        #get_rpt_Suggestion_Delivered_stg has to be patched differently to allow
        #for the return value to be a DataFrame
        with patch.object(RepEngagementCalculator, "get_rpt_Suggestion_Delivered_stg") as mock_get_rpt_Suggestion_Delivered_stg:
            RepEngagementCalculator.get_rpt_Suggestion_Delivered_stg.return_value = mock_table
            calc.finalize_df(1, 2, 3)
            mock_get_AKT_RepLicense_arc.assert_called()
            mock_get_rpt_dim_calendar.assert_called()
            mock_get_rpt_Suggestion_Delivered_stg.assert_called()
            mock_merge_suggestions_delivered_with_AKT_RepLicense_arc.assert_called()
            mock_merge_suggestions_delivered_RepLicense_with_rpt_dim.assert_called()
            mock_filter_holidays.assert_called()
            mock_add_colums_df.assert_called()
            mock_suggestions_delivered_calculation.assert_called()
            mock_engaged_suggestions_calculations.assert_called()
            mock_merge_suggestions_delivered_with_engaged.assert_called()

    def test_truncating_data(self):
        spark, calc = self.init_spark()

        #testing when repengagement is empty
        #function inputs
        field = [StructField("FIELDNAME_1",StringType(), True)]
        schema = StructType(field)
        mock_empty_rep_engagement_table = spark.createDataFrame([], schema)
        #the empty table above will act as an empty RepEngagementCalculation table
        #the full table is written below to ensure that the table doesn't get truncated
        super().write_table("repengagementToTruncate", "RepEngagementCalculation", "learning")
        today = datetime(2020, 1, 1, 12, 00, 1)

        #expected outputs
        repengagementNotTruncatedReference = self.pd_to_spark(spark, super().get_table("RepEngagementCalculation", self.learning_db_name), schemas.RepEngagementCalculation)
        # repengagementTruncatedReference = spark.read.schema(schemas.RepEngagementCalculation).json("sparkRepEngagementCalculator/tests/references/repengagement.json")

        #testing outputs
        #check that the table was correctly truncated
        calc.truncating_data(mock_empty_rep_engagement_table, today)
        repengagementNotTruncated = self.pd_to_spark(spark, super().get_table("RepEngagementCalculation", self.learning_db_name), schemas.RepEngagementCalculation)
        self.assertEqual(True, self.compare_df(repengagementNotTruncatedReference, repengagementNotTruncated))

        #testing when repengagement is not empty
        #function inputs
        today = datetime(2020, 1, 1, 12, 00, 1)
        repengagementNotTruncatedReference = self.pd_to_spark(spark, super().get_table("RepEngagementCalculation", self.learning_db_name), schemas.RepEngagementCalculation)

        #expected outputs
        repengagementTruncatedReference = spark.read.schema(schemas.RepEngagementCalculation).json("sparkRepEngagementCalculator/tests/references/repengagement.json")

        #testing outputs
        #check that the table was correctly truncated
        calc.truncating_data(repengagementNotTruncatedReference, today)
        repengagementTruncated = self.pd_to_spark(spark, super().get_table("RepEngagementCalculation", self.learning_db_name), schemas.RepEngagementCalculation)
        self.assertEqual(False, self.compare_df(repengagementNotTruncatedReference, repengagementTruncated))
        self.assertEqual(True, self.compare_df(repengagementTruncatedReference, repengagementTruncated))

    @patch("common.DataAccessLayer.SparkDataAccessLayer.SparkLearningAccessor.write_rep_engagement_table")
    def test_populating_RepEngagementTable(self, mock_write_rep_engagement_table):
        spark, calc = self.init_spark()

        #function inputs
        field = [StructField("FIELDNAME_1",StringType(), True)]
        schema = StructType(field)
        mock_rep_engagement_table = spark.createDataFrame([("a",)], schema)

        #expected outputs

        #testing outputs
        calc.populating_RepEngagementTable(mock_rep_engagement_table)
        mock_write_rep_engagement_table.assert_called_with(mock_rep_engagement_table)

    @patch.object(RepEngagementCalculator, "truncating_data", return_value = [1])
    @patch.object(RepEngagementCalculator, "populating_RepEngagementTable", return_value = [1])
    def test_populate_db(self, mock_populating_RepEngagementTable, mock_truncating_data):
        spark, calc = self.init_spark()

        #function inputs

        #expected outputs

        #testing outputs
        calc.populate_db(1, 2, 3)
        mock_truncating_data.assert_called()
        mock_populating_RepEngagementTable.assert_called()

    @patch.object(RepEngagementCalculator, "initialize_df", return_value=[1,2,3,4,5])
    @patch.object(RepEngagementCalculator, "finalize_df")
    @patch.object(RepEngagementCalculator, "populate_db")
    @patch.object(RepEngagementCalculator, "update_ChannelActionMap")
    def test_calculate(self, mock_update_ChannelActionMap, mock_populate_db, mock_finalize_df, mock_initialize_df):
        #tests that the calculate function calls the expected functions
        spark, calc = self.init_spark()
        calc.calculate()
        mock_initialize_df.assert_called()
        mock_finalize_df.assert_called()
        mock_populate_db.assert_called()
        mock_update_ChannelActionMap.assert_called()

    def reqDBs(self):
        required_dse = [ "DSERun", "DSERunRepDate", "SparkDSERun", "SparkDSERunRepDate", "Rep", "Interaction"]
        required_cs = [ "Sync_Tracking_vod__c" ]
        required_stage = [ "AKT_RepLicense_arc", "rpt_dim_calendar", "RPT_Suggestion_Delivered_stg" ]
        required_learning = [ "RepEngagementCalculation" ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required

class test_RepEngagementCalculatorMain(test_RepEngagementCalculator):

    def test_main_0_args(self):
        #test lines 388-390 for 0 (too few) arguments supplied
        sys.argv = []
        with self.assertRaises(SystemExit) as exitError:
            main()
        self.assertEqual(exitError.exception.code, 0)

    def test_main_11_args(self):
        #test lines 388-390 for 10 (too many) arguments supplied
        sys.argv = [1,2,3,4,5,6,7,8,9,10,11]
        with self.assertRaises(SystemExit) as exitError:
            main()
        self.assertEqual(exitError.exception.code, 0)

    @patch.object(RepEngagementCalculator, "calculate")
    def test_main_10_args(self, mock_calculate):
        #tests the main function when supplied with 9 (the expected number) of arguments
        sys.argv = [sys.argv[1][9:-1]+"/sparkRepEngagementCalculator/RepEngagementCalculator.py", self.db_host, self.db_user, self.db_password, "test", self.cs_db_name, self.db_port, sys.argv[1][9:-1], "unittest", "env"]
        main()
        mock_calculate.assert_called()

    def reqDBs(self):
        required_dse = [ "DSERun", "DSERunRepDate", "SparkDSERun", "SparkDSERunRepDate", "Rep", "Interaction"]
        required_cs = [ "Sync_Tracking_vod__c" ]
        required_stage = [ "AKT_RepLicense_arc", "rpt_dim_calendar", "RPT_Suggestion_Delivered_stg" ]
        required_learning = [ "RepEngagementCalculation" ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required
