import unittest
from unittest.mock import patch
import sys
import os

script_path = os.path.realpath(__file__)
script_dir = os.path.dirname(script_path)
module_dir = os.path.dirname(script_dir)
learning_dir = os.path.dirname(module_dir)
sys.path.append(learning_dir)

from common.unitTest.pyunittestUtils import unitTestParent
from [MODULE] import [CLASS]
from [MODULE] import [FUNCTION]

class test_[FILE](unitTestParent):

    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().tearDown()

class test_[CLASS](test_[FILE]):
#if not implementing a file parent function like test_[FILE], have each test class inherit from unitTestParent

    def test_one(self):
        #be sure all test functions start with "test_"!
        return

    def reqDBs(self):
        required_dse = [ "table_one", "table_two" ]
        required_cs = [ ]
        required_stage = [ ]
        required_learning = [ ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required

class test_[FUNCTION](test_[FILE]):

    def test_one(self):
        return

    def reqDBs(self):
        required_dse = [ ]
        required_cs = [ ]
        required_stage = [ ]
        required_learning = [ ]
        required_archive = [ ]
        required = {"dse" : required_dse, "cs" : required_cs, "stage" : required_stage, "learning" : required_learning, "archive" : required_archive}
        return required
