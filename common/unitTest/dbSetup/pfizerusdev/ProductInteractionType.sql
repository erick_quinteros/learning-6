CREATE TABLE `ProductInteractionType` (
  `productInteractionTypeId` tinyint(4) NOT NULL,
  `productInteractionTypeName` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productInteractionTypeId`),
  UNIQUE KEY `productInteractionType_uniqueName` (`productInteractionTypeName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci