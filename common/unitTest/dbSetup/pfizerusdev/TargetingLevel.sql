CREATE TABLE `TargetingLevel` (
  `targetingLevelId` int(11) NOT NULL AUTO_INCREMENT,
  `externalId` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `targetingLevelsPeriodId` int(11) NOT NULL,
  `targetingLevelName` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `repActionTypeId` tinyint(4) NOT NULL,
  `facilityRule` tinyint(2) NOT NULL,
  `accountGroupRule` tinyint(2) NOT NULL,
  `accountRule` tinyint(2) NOT NULL,
  `repTeamRule` tinyint(2) NOT NULL,
  `repRule` tinyint(2) NOT NULL,
  `productRule` tinyint(2) NOT NULL,
  `messageTopicRule` tinyint(2) NOT NULL,
  `messageRule` tinyint(2) NOT NULL,
  `rollupTargetingLevelId` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `subordinateTargetingLevelIds` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`targetingLevelId`),
  UNIQUE KEY `targetingLevel_uniqueExternalId` (`externalId`),
  KEY `targetingLevel_targetingLevelsPeriodId` (`targetingLevelsPeriodId`),
  KEY `targetingLevel_repActionTypeId` (`repActionTypeId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci