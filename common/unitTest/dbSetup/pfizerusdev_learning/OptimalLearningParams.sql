CREATE TABLE `OptimalLearningParams` (
  `productUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `channelUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `goal` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `paramName` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `paramValue` longtext COLLATE utf8_unicode_ci,
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`productUID`,`channelUID`,`goal`,`paramName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
