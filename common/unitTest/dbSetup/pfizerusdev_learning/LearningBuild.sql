CREATE TABLE `LearningBuild` (
  `learningBuildUID` varchar(80) NOT NULL,
  `learningVersionUID` varchar(80) NOT NULL,
  `learningConfigUID` varchar(80) NOT NULL,
  `isDeployed` tinyint(1) NOT NULL DEFAULT '0',
  `executionStatus` varchar(20) DEFAULT NULL,
  `executionDatetime` datetime DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`learningBuildUID`),
  KEY `learningBuild_fk_1_idx` (`learningConfigUID`),
  KEY `learningBuild_fk_2_idx` (`learningVersionUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8