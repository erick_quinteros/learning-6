CREATE TABLE `AKT_Message_Topic_Email_Learned` (
  `messageUID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `productUID` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL,
  `documentDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailSubject` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailTopicId` int(11) DEFAULT NULL,
  `emailTopicName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci