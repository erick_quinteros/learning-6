CREATE TABLE `RepDateLocation` (
`repId` int(11) NOT NULL,
`date` date NOT NULL,
`latitude` double NOT NULL,
`longitude` double NOT NULL,
`probability` double DEFAULT NULL, 
`maxNearDistance` double DEFAULT NULL,
`maxFarDistance` double DEFAULT NULL,
`source` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
`runDate` date NOT NULL,
`learningBuildUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
`learningRunUID` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
`createdAt` datetime DEFAULT CURRENT_TIMESTAMP,
`updatedAt` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
PRIMARY KEY (`repId`,`date`,`latitude`,`longitude`,`source`,`learningRunUID`),
KEY `idx_repId_date` (`date`,`repId`),
KEY `RepDateLocation_fk_1` (`learningRunUID`),
KEY `RepDateLocation_fk_2` (`learningBuildUID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;