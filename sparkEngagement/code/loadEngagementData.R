##########################################################
#
#
# aktana- engagement estimates estimates Aktana Learning Engines.
#
# description: read the data
#
# created by : marc.cohen@aktana.com
# updated by : wendong.zhu@aktana.com
#
# updated on : 2019-03-12
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################

library(data.table)
library(futile.logger)
library(sparklyr)
library(dplyr)
library(sparkLearning)

loadEngagementData <- function(sc, sparkDBconURL, sparkDBconURL_stage, startHorizon, useForProbability)
{
        
    loadedData <- list()

    # dbGetQuery(con,"SET NAMES utf8")

    # read the interactions table
    flog.info("read Interaction table")
    interactionsSQL <- sprintf("select * from Interaction where startDateLocal>'%s'", startHorizon)
    interactions <- sparkReadFromDB(sc, sparkDBconURL, interactionsSQL, name="interactions", partitionColumn="interactionId")
    #interactions[,eventdate:=as.Date(startDateLocal)]
    #interactions <- mutate(interactions, eventdate = as.Date(startDateLocal))
    # interactions <- interactions %>% mutate(eventdate = to_date(startDateLocal)) %>% select(-startDateLocal)
    interactions <- dplyr::rename(interactions, eventdate = startDateLocal)
      
    # read the interaction account table
    flog.info("read InteractionAccount table")
    interactionAccountSQL <- "select interactionId, accountId from InteractionAccount"
    interactionAccount <- sparkReadFromDB(sc, sparkDBconURL, interactionAccountSQL, name="interactionAccount", partitionColumn="interactionId")

    #interactions <- merge(interactions,interactionAccount,by="interactionId") # merge with the interactions
    interactions <- interactions %>% inner_join(interactionAccount, by="interactionId")

    # read the account table
    flog.info("read Account table")
    accounts <- sparkReadFromDB(sc, sparkDBconURL, "select accountId, externalId from Account", name="accounts", partitionColumn="accountId")
    #setnames(accounts,"externalId","accountUID")
    accounts <- dplyr::rename(accounts, accountUID = "externalId")
    #accounts <- sdf_copy_to(sc, accounts, "accounts", overwrite=TRUE, memory=FALSE)

    # read the interaction product table
    #interactionProduct <- data.table(dbGetQuery(con,sprintf("select interactionId, productId from %s.InteractionProduct;",schema)))

    # read the rep table
    flog.info("read Rep table")
    reps <- sparkReadFromDB(sc, sparkDBconURL, "select repId, externalId from Rep", name="reps", partitionColumn="repId")
    #setnames(reps,"externalId","repUID")
    reps <- dplyr::rename(reps, repUID = "externalId")
    #reps <- sdf_copy_to(sc, reps, "reps", overwrite=TRUE, memory=FALSE)

    flog.info("read repAssignments table")
    #repAssignments <- dbGetQuery(con,"SELECT repId, accountId FROM RepAccountAssignment;")
    repAssignments <- sparkReadFromDB(sc, sparkDBconURL, "SELECT repId, accountId FROM RepAccountAssignment", name="repAssignments", partitionColumn="repId")
    
    # from RepAccountAssignment, get only accounts existing in Rep table
    #accts <- unique(repAssignments[repId %in% reps$repId]$accountId)
    # repIds <- pull(reps, repId)
    # accts <- unique(repAssignments %>% filter(repId %in% repIds) %>% pull(accountId))
    accts <- repAssignments %>% 
      inner_join(select(reps, repId), by="repId") %>% 
      select(accountId) %>% 
      distinct()

    #interactions <- interactions[accountId %in% accts]    # filtering interactions using accountId
    # interactions <- interactions %>% filter(accountId %in% accts)
    interactions <- interactions %>% inner_join(accts, by="accountId")

    # update accounts and reps using RepAccountAssignment
    #accounts <- accounts[accountId %in% accts]
    # accounts <- accounts %>% filter(accountId %in% accts)
    accounts <- accounts %>% inner_join(accts, by="accountId")

    #repIds <- unique(repAssignments[repId %in% reps$repId]$repId)
    # repIds <- unique(repAssignments %>% filter(repId %in% repIds) %>% pull(repId))
    repIds <- repAssignments %>% select(repId) %>% distinct()

    #reps <- reps[repId %in% repIds]
    reps <- reps %>% inner_join(repIds, by="repId")

    # flatten the interactions with merges of accounts and reps
    #interactions <- merge(interactions,accounts,by="accountId")
    interactions <- interactions %>% inner_join(accounts, by="accountId")

    #interactions <- merge(interactions,reps,by="repId")
    interactions <- interactions %>% inner_join(reps, by="repId")

    #interactions <- interactions[,c("accountUID","accountId","repUID","repId","eventdate","repActionTypeId"),with=F]
    interactions <- interactions %>% select(accountUID, accountId, repUID, repId, eventdate, repActionTypeId)

    #interactions$reaction <- "Touchpoint"   # prepare for merge with the suggestions tables
    interactions <- mutate(interactions, reaction = "Touchpoint") 

    #setnames(interactions,"eventdate","date")
    interactions <- dplyr::rename(interactions, date = "eventdate")

    # read the feedback table from the stage db
    flog.info("read stage db Suggestion_Feedback_vod__c_arc table")
    
    #feedback <- unique(data.table(dbGetQuery(con,sprintf("select RecordTypeId, LastModifiedDate, Suggestion_vod__c from %s;",table))))
    feedbackSQL <- "select Id, RecordTypeId, LastModifiedDate, Suggestion_vod__c from Suggestion_Feedback_vod__c_arc"
    feedback <- sparkReadFromDB(sc, sparkDBconURL_stage, feedbackSQL, name="feedback", partitionColumn="Id")
    feedback <- feedback %>% select(-Id) %>% distinct()

    #setnames(feedback,"LastModifiedDate","feedbackDate")
    feedback <- dplyr::rename(feedback, feedbackDate = LastModifiedDate)
    feedback <- feedback %>% mutate(feedbackDate = as.character(feedbackDate))

    # read the record types from the stage db
    flog.info("read stage db RecordType_vod__c_arc table")
    recordType <- sparkReadFromDB(sc, sparkDBconURL_stage, "select Id, Name from RecordType_vod__c_arc", name="recordType", partitionColumn="Id")

    #setnames(recordType,"Id","RecordTypeId")
    recordType <- dplyr::rename(recordType, RecordTypeId = "Id")

    #feedback <- merge(feedback,unique(recordType),by="RecordTypeId")    # merge the recordtype into the feedback table to flatten
    feedback <- feedback %>% inner_join(recordType %>% distinct(), by = "RecordTypeId")

    #setnames(feedback,c("Name"),c("reaction"))
    feedback <- dplyr::rename(feedback, reaction = "Name")

    #feedback[reaction=="Activity_Execution_vod",reaction:="Execution"]  # rename feedback keys
    feedback <- mutate(feedback, reaction = ifelse(reaction=="Activity_Execution_vod", "Execution", reaction))

    #feedback[reaction=="Dismiss_vod",reaction:="Dismiss"]
    feedback <- mutate(feedback, reaction = ifelse(reaction=="Dismiss_vod", "Dismiss", reaction))

    #feedback[reaction=="Mark_As_Complete_vod",reaction:="Complete"]
    feedback <- mutate(feedback, reaction = ifelse(reaction=="Mark_As_Complete_vod", "Complete", reaction))

    #interactions[, c("accountUID","repUID"):=NULL]  # more cleanup of uneeded fields
    interactions <- interactions %>% select (-c(accountUID, repUID))

    # read the suggestion table from the stage db
    flog.info("read stage db Suggestion_vod__c_arc table")

    table <- "Suggestion_vod__c_arc"
    suggestionsSQL <- sprintf("select Id, OwnerId, Account_vod__c, Record_Type_Name_vod__c, Suggestion_External_Id_vod__c, CreatedDate, Title_vod__c from %s where Call_Objective_Record_Type_vod__c='Suggestion_vod' and Call_Objective_From_Date_vod__c>'%s'", table, startHorizon)
    suggestions <- sparkReadFromDB(sc, sparkDBconURL_stage, suggestionsSQL, name="suggestions", partitionColumn="Id")

    #setnames(suggestions,c("Id","OwnerId","Account_vod__c","Record_Type_Name_vod__c","CreatedDate"),c("Suggestion_vod__c","repUID","accountUID","type","date"))
    oldnames <- c("Id","OwnerId","Account_vod__c","Record_Type_Name_vod__c","CreatedDate")
    newnames <- c("Suggestion_vod__c","repUID","accountUID","type","date")
    suggestions <- dplyr::rename_at(suggestions, vars(oldnames), ~ newnames) 

    #suggestions[type=="Call_vod",repActionTypeId:=3]                                                                  ############### Kludge - this is to match the keys used in the DSE DB
    suggestions <- suggestions %>% mutate(repActionTypeId = ifelse(type == "Call_vod", 3, NA))

    #suggestions[type=="Email_vod",repActionTypeId:=12]                                                                ############### Kludge - this is to match the keys used in the DSE DB
    suggestions <- suggestions %>% mutate(repActionTypeId = ifelse(type == "Email_vod", 12, repActionTypeId))

    #suggestions[type=="Call_vod" & regexpr("~r[[:digit:]]+~W~",Suggestion_External_Id_vod__c)>1,repActionTypeId:=13]  ############### Kludge - this is to match the keys used in the DSE DB
    ##TODO: add web interation type 13
    suggestions <- mutate(suggestions, repActionTypeId = ifelse(type == "Call_vod" & rlike(Suggestion_External_Id_vod__c, "~r[[:digit:]]+~W~"), 13, repActionTypeId))

    
    #suggestions[,date:=as.Date(date)]
    suggestions <- mutate(suggestions, date = as.Date(date))

    # merge the suggestions and associated feedback if any
    flog.info("merge suggestions and feedback")
    #suggestions <- merge(suggestions,feedback,by="Suggestion_vod__c",all.x=T)
    suggestions <- suggestions %>% left_join(feedback, by="Suggestion_vod__c")

    #suggestions[is.na(reaction),reaction:="Ignored"]
    suggestions <- mutate(suggestions, reaction = ifelse(is.na(reaction), "Ignored", reaction))

    # cleanup uneeded fields
    #suggestions[,c("Suggestion_vod__c","RecordTypeId","Name"):=NULL]
    suggestions <- select(suggestions, -c(Suggestion_vod__c, RecordTypeId))

    # flatten the suggestions table
    #suggestions <- merge(suggestions,accounts,by="accountUID")
    suggestions <- suggestions %>% inner_join(accounts, by="accountUID")

    #suggestions <- merge(suggestions,reps,by="repUID")
    suggestions <- suggestions %>% inner_join(reps, by="repUID")

    # more cleanup of uneeded fields
    #suggestions[,c("accountUID","repUID"):=NULL]
    suggestions <- suggestions %>% select(-c(accountUID, repUID))
    
    # cache to memory for later faster processing
    flog.info("caching interactions")
    interactions <- sdf_register(interactions, "interactions_after_loadEngagementData")
    tbl_cache(sc, "interactions_after_loadEngagementData")
    interactions <- tbl(sc, "interactions_after_loadEngagementData")
    
    flog.info("caching suggestions")
    suggestions <- sdf_register(suggestions, "suggestions_after_loadEngagementData")
    tbl_cache(sc, "suggestions_after_loadEngagementData")
    suggestions <- tbl(sc, "suggestions_after_loadEngagementData")

    loadedData[["interactions"]] <- interactions
    loadedData[["suggestions"]] <- suggestions

    flog.info("Done with loadEngagementData.")
    
    return(loadedData)
}
