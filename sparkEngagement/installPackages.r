##########################################################
#
#
# aktana-learning Install Aktana Learning Engines.
#
# description: install needed packages for engagement
#
#
# created by : marc.cohen@aktana.com
#
# created on : 2016-11-29
#
# Copyright AKTANA (c) 2016.
#
#
####################################################################################################################
#
# install Packages needed for Engagement code
# build function library used by the Engagement driver code
#
##########################################################
args <- commandArgs(TRUE)

##args is now a list of character vectors
## First check to see if arguments are passed.
## Then cycle through each element of the list and evaluate the expressions.
if(length(args)==0){
  print("No arguments supplied.")
  quit(save = "no", status = 1, runLast = FALSE)
}else{
  print("Arguments supplied.")
  for(i in 1:length(args)){
    eval(parse(text=args[[i]]))
    print(args[[i]]);
  }
}

options(downLoad.file.method="curl")

# install devtools to manage package version
install.packages("devtools",repos="https://cran.rstudio.com/")
library(devtools)

# packaged needed for all modules for spark running
needed <- c("dplyr","stringi","sparklyr","testthat")
install.packages(needed,repos="http://cran.rstudio.com")

# install packages needed by the Engagement functions
needed <- c("RMySQL","ks","foreach","doMC","mvtnorm","rgl","uuid","properties","reshape2")
install.packages(needed,repos="http://cran.rstudio.com")

# install specific version of data.table, h2o
install_version("data.table", version = "1.11.4", repos = "http://cran.us.r-project.org")

# # install spark
# library(sparklyr)
# spark_install(version = "2.3.1")

#
# This code builds the learning package and then installs it
#
# 
# # R CMD build learningPackage && R CMD INSTALL Learning_1.0.tar.gz
# shellCode <- sprintf("tar -cvf Learning.tar.gz %s/learningPackage",homedir)
# system(shellCode)
# # if(is.element("Learning",inst))remove.packages("Learning")
# install.packages("Learning.tar.gz", repos = NULL, type = "source")
# 
# # R CMD build sparkLearningPackage && R CMD INSTALL sparkLearning_1.0.tar.gz 
# shellCode <- sprintf("tar -cvf sparkLearning.tar.gz %s/sparkLearningPackage",homedir)
# system(shellCode)
# # if(is.element("sparkLearning",inst))remove.packages("sparkLearning")
# install.packages("sparkLearning.tar.gz", repos = NULL, type = "source")
